import React, { Component } from 'react';
import './login.css';

class Login extends Component {
    render() {
        return (
            <div >
                <div className="row bglogin" >
                <div className="col-sm-4"/>
                <div className="col-sm-4">
                    <div className="login">
                    <h6 style={{textAlign: 'center', marginBottom: '30px'}}>Login</h6>
                    <form>
                        <div className="form-group">
                        <input type="email" className="form-control" placeholder="Enter email" />
                        </div>
                        <div className="form-group">
                        <input type="password" className="form-control" placeholder="Password" />
                        </div>
                        <button type="submit" className="btn submit_login"><b>Login</b></button>
                        <hr />
                        <table style={{display: 'flex', justifyContent: 'center'}}>
                        <tbody><tr>
                            <td><a href style={{color:'#007bff'}}>Không thể đăng nhập?   </a></td>
                            <td><a href style={{color:'#007bff'}}>Đăng kí tài khoản</a></td>
                            </tr>
                        </tbody></table>
                    </form>
                    </div>
                </div>
                <div className="col-sm-4" />
                </div>
            </div>
        );
    }
}

export default Login;