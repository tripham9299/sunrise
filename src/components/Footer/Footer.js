import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <div>
                <div style={{height:'120px', backgroundColor:'black',color:'#838383', textAlign:"center", paddingTop:'20px'}}>
                    <p style={{display:'inline-block'}}>Call.(+84) 334930853</p>
                    &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;
                    <p style={{display:'inline-block'}}>Email.tripham9299@gmail.com</p>
                    <p>Copyright © 2020 - Sunrise Hotel</p>
                </div>
            </div>
        );
    }
}

export default Footer;