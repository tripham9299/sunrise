import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from './pages/Login/Login';
import Home from './pages/Home/Home';
import TopMenu from './components/TopMenu/TopMenu';
import Room from './pages/Room/Room';
import Register from './pages/Register/Register';
import Detail from './pages/Detail/Detail';
import About from './pages/About/About';
import Admin from './pages/Admin/Admin';
import RoomEditing from './componentsAdmin/RoomEditing/RoomEditing';
// import Detail from './components/Detail/Detail';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
        <TopMenu/>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/home" exact component={Home} />
            <Route path="/room" exact component={Room} />
            <Route path="/about" exact component={About} />
            <Route path="/admin" exact component={Admin} />
            <Route path="/login" exact component={Login} />
            <Route path="/register" exact component={Register} />
            <Route path="/detail/:id" exact component={Detail} />
            <Route path="/roomEditing" exact component={RoomEditing}/>
          </Switch>
        </div>
     </Router>
    );
  }
}

export default App;
