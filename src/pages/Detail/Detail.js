import React, { Component } from 'react';
import {Row,Col, Button } from "reactstrap";
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import './Detail.css';
import Service from '../../components/Service/Service';
class Detail extends Component {
    constructor(props){
        super(props);
        console.log(this.props.match.params.id);
    }
    render() {
        return (
            <div>
                <Header backgr="url('/img/header.jpg')" welcome="Room Detail"/>
                <div className="container-lg">
                <div className="row" style={{marginTop:'90px'}}>
                    <div className= "col-lg-8">
                        <img className="imgRoom" src="/img/room1.jpg" alt="room" ></img>
                    </div>
                    <div className="col-lg-4 reservation">
                        <h3>YOUR RESERVATION</h3>
                        <form>
                            
                            <Row className="title">CHECK-IN</Row>
                            <Row><input type="date" /></Row>
                               
                            <Row className="title">CHECK-OUT</Row>
                            <Row><input type="date" /></Row>

                            <Row>
                                <Col lg="6">
                                    <Row className="title">NIGHT</Row>
                                    <Row>
                                        <select>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                            <option>10</option>
                                        </select>
                                    </Row>
                                </Col>
                                <Col lg="6">
                                    <Row className="title">ROOM</Row>
                                    <Row>
                                        <select>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                            <option>10</option>
                                        </select>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col lg="6">
                                    <Row className="title">ADULTS</Row>
                                    <Row>
                                        <select>
                                            <option>0</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </Row>
                                </Col>
                                <Col lg="6">
                                    <Row className="title">CHILDREN</Row>
                                    <Row>
                                        <select>
                                            <option>0</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </Row>
                                </Col>
                            </Row>
                            <Button type="submit" className="booknow">BOOK NOW</Button>
                        </form>
                    </div>
                    <div className= "col-lg-8">
                        <h2 className="roomType">Double Rooms</h2>
                        <h5 className="priceDetail">$82.34<span className="perNight1">per night</span></h5>
                        <hr/><br/><br/>
                        <b marginTop="60px">DESCRIPTION</b>
                        <p className="desDetail">
                            Pellentesque habitant morbi tristique senectus et netus et malesuada fames
                            ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, 
                            tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean 
                            ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est 
                            et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo
                            vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum,
                            eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in 
                            turpis pulvinar facilisis.
                        </p>
                    </div>
                    <div className="col-lg-4">
                        <h5 style={{paddingTop:"60px",paddingLeft:"40px"}} >Room Amenities</h5>
                        <ul className="amenities">
                            <li ><i color="#b19261" className="fa fa-check-circle"></i> Private bathroom</li>
                            <li ><i color="#b19261" className="fa fa-check-circle"></i> Free Wifi</li>
                            <li ><i color="#b19261" className="fa fa-check-circle"></i> Beach View</li>
                            <li ><i color="#b19261" className="fa fa-check-circle"></i> Free Lunch</li>
                            <li ><i color="#b19261" className="fa fa-check-circle"></i> Hot/Cold Shower & Bathtub</li>
                            <li ><i color="#b19261" className="fa fa-check-circle"></i> Writing Desk & Chair</li>
                            <li ><i color="#b19261" className="fa fa-check-circle"></i> 2 pair of slippers</li>
                            <li ><i color="#b19261" className="fa fa-check-circle"></i> Bottled Mineral Water</li>
                        </ul>
                    </div>
                </div>
                </div>
                <Service/>
                <Footer/>
            </div>
        );
    }
}

export default Detail;