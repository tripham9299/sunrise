import React, { Component } from 'react';
import Header from '../../components/Header/Header';
import UnderHeder from '../../components/Header/UnderHeder';
import CardRoom from '../../components/CardRoom/CardRoom';
import Footer from '../../components/Footer/Footer';
import Service from '../../components/Service/Service';

class Home extends Component {
    render() {
        return (
            <div>
                <Header backgr="url('img/header.jpg')" welcome="Welcome to Sunrise Hotel" underwelcome="Where every stay is unique."/>
                <UnderHeder/>
                <CardRoom/>
                <Service/>
                <Footer/>
            </div>
        );
    }
}

export default Home;