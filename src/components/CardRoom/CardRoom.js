import React, { Component } from 'react';
import {Container,Row,Col,CardText,CardBody,Button } from "reactstrap";
import {  Link } from 'react-router-dom';

class CardRoom extends Component {
    constructor(props) {
        super(props);
        this.state = {
          rooms: [
            {
                "id": "b26b7ff3-4921-4766-8de7-a7c76a69b32a",
                "type": "ZaamDox",
                "description": "in tempus sit amet sem fusce consequat nulla nisl nunc",
                "price": "$44.18",
                "img": "https://colorlib.com/preview/theme/travelix/images/offer_1.jpg",
                "number": 101
              }, {
                "id": "eac74671-039a-435e-8fad-536d4a4057da",
                "type": "Bitwolf",
                "description": "natoque penatibus et magnis dis parturient montes nasce",
                "price": "$26.69",
                "img": "https://colorlib.com/preview/theme/travelix/images/offer_3.jpg",
                "number": 102
              }, {
                "id": "c7e1616a-3dc0-4599-8ada-19999abab601",
                "type": "Voltsillam",
                "description": "feugiat non pretium quis lectus suspendisse potenti in ",
                "price": "$19.07",
                "img": "https://colorlib.com/preview/theme/travelix/images/offer_2.jpg",
                "number": 103
              }, {
                "id": "0e925a3a-a9ba-42f7-8881-b346eab31d0c",
                "type": "Zoolab",
                "description": "orci eget orci vehicula condimentum curabitur in",
                "price": "$59.86",
                "img": "img/offer_5.jpg",
                "number": 104
              }, {
                "id": "4a27baf3-5a77-46af-a88f-7c41b495ebf7",
                "type": "Temp",
                "description": "cursus urna ut tellus nulla ut erat id mauris vulputate",
                "price": "$67.53",
                "img": "img/room6.jpg",
                "number": 201
              }, {
                "id": "b3357f17-3b12-491d-b288-960cc9a0847a",
                "type": "Wrapsafe",
                "description": "turpis sed ante vivamus tortor duis mattis egestas metus",
                "price": "$66.47",
                "img": "img/room5.jpg",
                "number": 202
              }, {
                "id": "112f0972-c33a-4cd1-8ef8-63ec21407991",
                "type": "Yfind",
                "description": "bibendum morbi non quam nec dui luctus rutrum nulla tell",
                "price": "$69.55",
                "img": "img/room4.jpg",
                "number": 203
              }, {
                "id": "fd64d5cf-2e5d-406f-99f6-3d5f79f4e06e",
                "type": "Itwn",
                "description": "nulla nisl nunc nisl duis bibendum felis sed interdum",
                "price": "$55.73",
                "img": "img/room1.jpg",
                "number": 204
              }, {
                "id": "1b536456-5c9c-4e44-9be6-e1f6b70fee65",
                "type": "QuoLux",
                "description": "vel accumsan tellus nisi eu orci mauris lacinia sapien qui",
                "price": "$41.84",
                "img": "img/room3.jpg",
                "number": 301
              }, {
                "id": "44621f48-fe63-40cb-b131-01fa29bb8a07",
                "type": "Andalax",
                "description": "magna bibendum imperdiet nullam orci pede venenatis",
                "price": "$82.34",
                "img": "img/room2.jpg",
                "number": 302
              }]
        };
      }

    render() {
        const { rooms } = this.state;
        return (
          <Container>
          <Row>
            {rooms.map(room => (
              <Col lg="6" style={{marginTop: '90px'}}>
              <Row>
                  <Col lg="6">
                    <img className="imageRoom" src={room.img} alt=""/>
                    <div className="typeRoom">
                      <p >{room.type}-{room.number}</p>
                    </div>
                  </Col>
                  <Col lg="6">
                  <CardBody>
                    <h5 className="price">{room.price}</h5>
                    <span className="perNight">per night</span>
                    <img className="rating" src="img/rating.jpg" alt=""/>
                    <CardText className="description">{room.description}</CardText>
                    <ul style={{paddingLeft:'0'}} >
                      <li className="icon"><i className="fa fa-coffee"></i></li>
                      <li className="icon"><i className="fa fa-wifi"></i></li>
                      <li className="icon"><i className="fa fa-utensils"></i></li>
                      <li className="icon"><i className="fa fa-plane"></i></li>
                    </ul>
                    <Link to={ "/detail/"+ room.id }><Button className="seaMore"> SEA MORE</Button></Link>
                   
                  </CardBody>
                  </Col>
                </Row>
              </Col>
            ))}
          </Row>
        </Container>
        );
    }
}

export default CardRoom;