import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
class TopMenu extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav" style={{backgroundColor:'#212529'}}>
                    <div className="container">
                    <a className="navbar-brand js-scroll-trigger" href="#page-top"><h5>Sunrise Hotel</h5></a>
                    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">Menu<i className="fas fa-bars ml-1" /></button>
                    <div className="collapse navbar-collapse" id="navbarResponsive">
                        <ul className="navbar-nav text-uppercase ml-auto">
                        <li className="nav-item"><NavLink to="/home" className="nav-link js-scroll-trigger">Home</NavLink></li>
                        <li className="nav-item"><NavLink to="/room" className="nav-link js-scroll-trigger">Room</NavLink></li>
                        <li className="nav-item"><NavLink to="/about" className="nav-link js-scroll-trigger">About us</NavLink></li>
                        <li className="nav-item"><NavLink to="/contact" className="nav-link js-scroll-trigger">Contact</NavLink></li>
                        <li className="nav-item"><NavLink to="/admin" className="nav-link js-scroll-trigger">Admin</NavLink></li>
                        <li className="nav-item"><NavLink to="/login" className="nav-link js-scroll-trigger">Login</NavLink></li>
                        <li className="nav-item"><NavLink to="/register" className="nav-link js-scroll-trigger">Register</NavLink></li>
                        </ul>
                    </div>
                    </div>
                </nav>
            </div>
        );
    }
}

export default TopMenu;