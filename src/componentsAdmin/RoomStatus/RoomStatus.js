import React, { Component } from 'react';
import {Row,Col,Button } from "reactstrap";
import classNames from 'classnames';
import "./RoomStatus.css";

class RoomStatus extends Component {
    constructor(props){
        super(props);
        this.state={
            roomStatus:[
                {
                    "id": "b26b7ff34766-8d",
                    "userid":"fhskfhk888",
                    "username":"Itachi",
                    "checkin":"04/06/2020",
                    "checkout":"05/06/2020",
                    "phone":"0918273645",
                    "status":"1",
                    "adults":2,
                    "children":0,
                    "number": 101
                  },
                  {
                    "id": "b26b7ff3-4921-76a69b32a",
                    "userid":"fhsjlgdfhk8968",
                    "username":"Tom Cruise",
                    "checkin":"04/06/2020",
                    "checkout":"05/06/2020",
                    "phone":"0918273645",
                    "status":"1",
                    "adults":1,
                    "children":1,
                    "number": 102
                  },
                  {
                    "id": "66-8de7-a7c76a69b32a",
                    "userid":"fhskfhk888",
                    "username":"Porter",
                    "checkin":"09/06/2020",
                    "checkout":"11/06/2020",
                    "phone":"0918273645",
                    "status":"2",
                    "adults":2,
                    "children":1,
                    "number": 103
                  },
                  {
                    "id": "b2-8de7-a7c76a69b32a",
                    "userid":"fhskfhk888",
                    "username":"",
                    "checkin":"",
                    "checkout":"",
                    "phone":"",
                    "status":"0",
                    "adults":0,
                    "children":0,
                    "number": 104
                  },
                  {
                    "id": "b2ff3-49b32a",
                    "userid":"fhskfhk888",
                    "username":"Matherin",
                    "checkin":"10/06/2020",
                    "checkout":"12/06/2020",
                    "phone":"0918273645",
                    "status":"2",
                    "adults":2,
                    "children":0,
                    "number": 201
                  },
                  {
                    "id": "bff3-49218de7-b32a",
                    "userid":"fhskfhk888",
                    "username":"Styther",
                    "checkin":"04/06/2020",
                    "checkout":"05/06/2020",
                    "phone":"0918273645",
                    "status":"1",
                    "adults":2,
                    "children":0,
                    "number": 202
                  },
                  {
                    "id": "b26b7f-a7c76a69b32a",
                    "username":"",
                    "checkin":"",
                    "checkout":"",
                    "phone":"",
                    "status":"0",
                    "adults":0,
                    "children":0,
                    "number": 203
                  },
                  {
                    "id": "b26b7e7-a7c76a69b32a",
                    "userid":"fhskfhk888",
                    "username":"Dumberdor",
                    "checkin":"04/06/2020",
                    "checkout":"05/06/2020",
                    "phone":"0918273645",
                    "status":"1",
                    "adults":2,
                    "children":0,
                    "number": 204
                  },
                  {
                    "id": "b26a7c76a69b32a",
                    "username":"",
                    "checkin":"",
                    "checkout":"",
                    "phone":"",
                    "status":"0",
                    "adults":0,
                    "children":0,
                    "number": 301
                  },
                  {
                    "id": "b26b7ff3-6a69b32a",
                    "userid":"fhskfhk8ggs5688",
                    "username":"Voldermort",
                    "checkin":"14/06/2020",
                    "checkout":"15/06/2020",
                    "phone":"0918273645",
                    "status":"2",
                    "adults":2,
                    "children":0,
                    "number": 302
                  },
            ]
        };
    }
    render() {
        const {roomStatus}= this.state;
        return (
            <div className="container" style={{marginTop:'70px', color:'rgb(62, 85, 105)'}}>
            <h3>Room status</h3>
            <Row >
              <Col sm='4'><div className="note" style={{backgroundColor:'#2db300'}}>Empty</div></Col>
              <Col sm='4'><div className="note" style={{backgroundColor:'#0090ff'}}>Has placed</div></Col>
              <Col sm='4'><div className="note" style={{backgroundColor:'#ff3333'}}>Using</div></Col>
            </Row>
            <Row>
                {roomStatus.map( item => (
                    <Col lg="4">
                      <div style={{marginLeft:'12px', marginRight:'12px',marginTop:'25px'}}>
                        <Row>
                            <Col lg="4" className={classNames('roomnum', {'zezo':item.status=="0"},{'one':item.status=="1"} ,{'two':item.status=="2"})} >
                              {item.number}
                            </Col>
                            <Col lg="8" className={classNames( {'zezo1':item.status=="0"},{'one1':item.status=="1"} ,{'two1':item.status=="2"})} >
                              <p></p>
                            </Col>
                        </Row>
                      </div>
                    </Col>
                ))}
            </Row>
            </div>
        );
    }
}

export default RoomStatus;