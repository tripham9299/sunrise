import React, { Component } from 'react';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Service from '../../components/Service/Service';

class Room extends Component {
    render() {
        return (
            <div>
                <Header backgr="url('img/header1.jpg')" welcome="Welcome to Sunrise Hotel" underwelcome="Where every stay is unique."/>
                <Service/>
                <Footer/>
            </div>
        );
    }
}

export default Room;