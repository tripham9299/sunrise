import React, { Component } from 'react';
import Dashboard from '../../componentsAdmin/Dashboard/Dashboard';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import RoomEditing from '../../componentsAdmin/RoomEditing/RoomEditing';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Service from '../../components/Service/Service';
import RoomStatus from '../../componentsAdmin/RoomStatus/RoomStatus';

class Admin extends Component {
    render() {
        return (
            <div>
                <Dashboard/>
                <RoomStatus/>
                <div style={{height:'275px'}}></div>
                <Service/>
                <Footer/>
            </div>
        );
    }
}

export default Admin;