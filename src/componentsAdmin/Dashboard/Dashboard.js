import React, { Component } from 'react';
import {Row,Col,Button } from "reactstrap";
import { Link } from 'react-router-dom';
import './Dashboard.css';
class Dashboard extends Component {
    render() {
        return (
            <div>
                <div className="container">
                    <h5 style={{marginTop:'120px',marginBottom:'30px',color:'#3e5569'}}>Dashboard</h5>
                    <Row>
                        <Col sm="2" >
                            <div className='dash' style={{backgroundColor:'#27a9e3'}}>
                                <Link to="/admin" style={{textDecoration:'none'}}>
                                <div><i className="fas fa-columns icondash" ></i></div>
                                <span>Dashboard</span>
                                </Link>
                            </div>
                        </Col>
                        <Col sm="4" >
                            <div className='dash' style={{backgroundColor:'#28b779'}}>
                                <div><i className="fas fa-map icondash" ></i></div>
                                <span>Room map</span>
                            </div>
                        </Col>
                        <Col sm="2" >
                            <div className='dash' style={{backgroundColor:'#ffb848'}}>
                                <Link to="roomEditing" style={{textDecoration:'none'}} >
                                <div><i className="fas fa-edit icondash" ></i></div>
                                <span>Room editing</span>
                                </Link>
                            </div>
                        </Col>
                        <Col sm="2" >
                            <div className='dash' style={{backgroundColor:'#da542e'}}>
                                <div><i className="fas fa-users icondash" ></i></div>
                                <span>User manager</span>
                            </div>
                        </Col>
                        <Col sm="2" >
                            <div className='dash' style={{backgroundColor:'#2255a4'}}>
                                <div><i className="fab fa-elementor icondash" ></i></div>
                                <span>Reservation</span>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

export default Dashboard;