import React, { Component } from 'react';

class Header extends Component {
    render() {
        const {backgr, welcome, underwelcome }= this.props;
        return (
            <div>
                <header className="masthead" style={{backgroundImage:backgr}}>
                    <div className="overlay"></div>
                    <div className="container">
                    <div className="masthead-heading ">{welcome}</div>
                    <div className="masthead-subheading">{underwelcome}</div>
                    <a className="btn btn-primary btn-xl text-uppercase js-scroll-trigger" style={{position:'relative'}} href="#services">Explore</a>
                    </div>
                </header>
            </div>
        );
    }
}

export default Header;