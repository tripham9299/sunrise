import React, { Component } from 'react';
import {Button } from "reactstrap";
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import './About.css';
import Service from '../../components/Service/Service';
class About extends Component {
    render() {
        return (
            <div>
                <Header backgr="url('img/about.jpg')" welcome="About us"/>
                <div className="row" style={{margin:'120px 50px 30px 50px'}}>
                    <div className="col-lg-7">
                        <img src="img/intro.png" alt=""></img>
                    </div>
                    <div className="col-lg-5">
                        <div style={{fontSize:'30px', fontWeight:'700', paddingBottom:'40px'}}>WE HAVE THE BEST SERVICES</div>

                        <p className="intro_text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus quis
                        vulputate eros, iaculis consequat nisl. Nunc et suscipit urna. Integer 
                        elementum orci eu vehicula pretium. Donec bibendum tristique condimentum.
                        Aenean in lacus ligula. Phasellus euismod gravida eros. Aenean nec ipsum 
                        aliquet, pharetra magna id, interdum sapien. Etiam id lorem eu nisl pellentesque
                        semper. Nullam tincidunt metus placerat, suscipit leo ut, tempus nulla. 
                        Fusce at eleifend tellus. Ut eleifend dui nunc, non fermentum quam placerat non.
                        Etiam venenatis nibh augue, sed eleifend justo tristique eu</p>
                        <Button className="explore"> EXPLORE NOW</Button>
                    </div>
                </div>
                <Service/>
                <Footer/>
            </div>
        );
    }
}

export default About;