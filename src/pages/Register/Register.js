import React, { Component } from 'react';
import './Register.css';

class Register extends Component {
    render() {
        return (
            <div className="bglogin">
                <div className="row">
                    <div className="col-sm-4"/>
                    <div className="col-sm-4">
                        <div className="login">
                        <h6 style={{textAlign: 'center', marginBottom: '30px'}}>Register</h6>
                        <form>
                            <div className="form-group">
                            <input type="name" name="name" className="form-control" placeholder="Enter name" />
                            </div>
                            <div className="form-group">
                            <input type="email" name="email" className="form-control" placeholder="Enter email" />
                            </div>
                            <div className="form-group">
                            <input type="password" name="password" className="form-control" placeholder="Password" />
                            </div>
                            <div className="form-group">
                            <input type="password" name="password" className="form-control" placeholder="Phone number" />
                            </div>
                            <p style={{fontSize: '12px', color: '#5E6C84'}}>Bằng cách đăng kí, bạn xác nhận bạn đã đọc và chấp nhận <a href style={{color:'#009900'}}>Điều khoản dịch vụ </a> và <a href style={{color:'#009900'}}>Chính sách riêng tư </a>của chúng tôi.</p>
                            <button type="submit" className="btn submit_login"><b>Register</b></button>
                            <hr />
                            <table style={{display: 'flex', justifyContent: 'center'}}>
                            <tbody><tr>
                                <td><a href style={{color:'#007bff'}}>Bạn đã có tài khoản?   </a></td>
                                <td><a href style={{color:'#007bff'}}>Đăng nhập</a></td>
                                </tr>
                            </tbody></table>
                        </form>
                        </div>
                    </div>
                    <div className="col-sm-4"/>
                   
                </div>
            </div>
        );
    }
}

export default Register;