import React, { Component } from 'react';

class UnderHeder extends Component {
    render() {
        return (
            <div className="container">
                <div ><h3 className="underHeader"> Hotel Master Rooms</h3></div>
                <hr style={{width: '25%', paddingBottom:'10px'}}></hr>
            </div>
        );
    }
}

export default UnderHeder;