import React, { Component } from 'react';

class Service extends Component {
    render() {
        return (
            <div>
                <div>
                    <h2 style={{ marginTop:'70px', marginBottom:'10px', textAlign:'center', fontFamily:'Crimson Text',
                    fontSize:'40px',fontWeight:'600'}}>Our Services</h2>
                    <p style={{textAlign:'center',color: '#5a5a5a'}}>Sunrise Hotel provides all services you need.</p>
                    <hr style={{ width:'10%',marginTop:'40px', marginBottom:'20px'}}></hr>
                </div>
                <div className="row" style={{textAlign:'center', fontFamily:'Crimson Text', marginBottom:'70px'}}>
                    <div className="col-sm-3" >
                        <div style={{display:'flex', justifyContent:'center'}}>
                            <img src=" /img/swim.png" alt="" style={{marginTop:'40px', marginBottom:'40px'}}  />
                        </div>
                        <p style={{ fontWeight:'600',color: '#2a2a2a', fontSize:'24px'}}>Swimming Pool</p>
                        <p style={{color:'#5a5a5a',fontWeight:'400', lineHeight:'1.8em', paddingLeft:'20%',paddingRight:'20%'}}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                    </div>
                    <div className="col-sm-3" >
                        <div style={{display:'flex', justifyContent:'center'}}>
                            <img src=" /img/wifi.png" alt="" style={{marginTop:'40px', marginBottom:'40px'}}  />
                        </div>
                        <p style={{ fontWeight:'600',color: '#2a2a2a', fontSize:'24px'}}>Free Wifi</p>
                        <p style={{color:'#5a5a5a',fontWeight:'400', lineHeight:'1.8em', paddingLeft:'20%',paddingRight:'20%'}}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                    </div>
                    <div className="col-sm-3" >
                        <div style={{display:'flex', justifyContent:'center'}}>
                            <img src=" /img/taxi.png" alt="" style={{marginTop:'40px', marginBottom:'40px'}}  />
                        </div>
                        <p style={{ fontWeight:'600',color: '#2a2a2a', fontSize:'24px'}}>Airport Taxi</p>
                        <p style={{color:'#5a5a5a',fontWeight:'400', lineHeight:'1.8em', paddingLeft:'20%',paddingRight:'20%'}}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                    </div>
                    <div className="col-sm-3" >
                        <div style={{display:'flex', justifyContent:'center'}}>
                            <img src=" /img/food.png" alt="" style={{marginTop:'40px', marginBottom:'40px'}}  />
                        </div>
                        <p style={{ fontWeight:'600',color: '#2a2a2a', fontSize:'24px'}}>Breakfast</p>
                        <p style={{color:'#5a5a5a',fontWeight:'400', lineHeight:'1.8em', paddingLeft:'20%',paddingRight:'20%'}}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod.</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Service;